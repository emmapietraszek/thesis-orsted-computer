/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DeterministicModel;


import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author XEPIE
 */
public class DeterministicModel {
       
    private final IloCplex model;
    private final DeterministicProblem dp;
    private final IloIntVar x[][];
    private final IloNumVar k[][][];
    
    public DeterministicModel(DeterministicProblem dp) throws IloException{
        
        //Creating a object for the model and the problem
        this.model = new IloCplex();
        model.setParam(IloCplex.Param.MIP.Tolerances.MIPGap,1e-02); //one percent         
        this.dp = dp;
        
        //Create desition variables
        x = new IloIntVar[dp.getNBundles()][dp.getNWeeks()];
        k = new IloNumVar[dp.getNResources()][dp.getNGroupedRes()][dp.getNWeeks()];
        
        //Initializing the variables
        for(int i=1; i <= dp.getNBundles(); i++){
            for(int t=1; t <= dp.getNWeeks(); t++){
                if(dp.getITindicator(i,t)==true){ //I only create the varibales where the indicator is true
                    x[i-1][t-1] = model.boolVar();
                }
            }
        }    
        for(int r=1; r <= dp.getNResources(); r++){
            for(int n=1; n <= dp.getNGroupedRes(); n++){
                if(dp.getRNindicator(r, n)==true){
                    for(int t=1; t <= dp.getNWeeks(); t++){
                        k[r-1][n-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
                    }
                }                
            }        
        }
        
        //Objective funciton
        IloLinearNumExpr obj = model.linearNumExpr();
        for(int i=1; i <= dp.getNBundles(); i++){
            for(int t=1; t <= dp.getNWeeks(); t++){
                if(dp.getITindicator(i, t)==true){            
                    obj.addTerm(dp.getValues(i, t), x[i-1][t-1]);
                }
            }
        }
        model.addMaximize(obj);
        
        //Constraints
        for(int j=1; j <= dp.getNTasks(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i=1; i <= dp.getNBundles(); i++){
                for(int t=1; t <= dp.getNWeeks(); t++){
                    if(dp.getITindicator(i, t)==true){
                        lhs.addTerm(dp.getA(i, j), x[i-1][t-1]);
                    }
                }
            }
            model.addEq(lhs, 1);
        }
        for(int n=1; n <= dp.getNGroupedRes(); n++){
            for(int t=1; t <= dp.getNWeeks(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int r=1; r <= dp.getNResources(); r++){
                    if(dp.getRNindicator(r, n)==true){
                        lhs.addTerm(k[r-1][n-1][t-1], 1);
                    }
                }
                model.addLe(lhs, dp.getCapacities(n, t));
            }
        }
        for(int r=1; r <= dp.getNResources(); r++){
            for(int t=1; t <= dp.getNWeeks(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int i=1; i <= dp.getNBundles(); i++){
                    if(dp.getITindicator(i, t)==true){
                        lhs.addTerm(dp.getHours(i,r), x[i-1][t-1]);
                    }
                }
                for(int n=1; n <= dp.getNGroupedRes(); n++){
                    if(dp.getRNindicator(r, n)==true){
                        lhs.addTerm(k[r-1][n-1][t-1], -1);
                    }
                }
                model.addLe(lhs, 0);
            }
        }  
    }
      
    //Solving method that prints the optimal solution
    public void solve() throws IloException{        
        model.setOut(null);
        model.solve();
        //System.out.println("Optimal objective value "+model.getObjValue());
        //System.out.println("Optimal solution ");
        /*
        int countTasks = 0; 
        int countPrev = 0;
        int countCorr = 0;
        int countMod = 0;
        */
        /*
        for(int i = 1; i<=dp.getNBundles(); i++){
            for(int t = 1; t < dp.getNWeeks(); t++){
                if(dp.getITindicator(i, t)==true && model.getValue(x[i-1][t-1]) > 0){
                    //System.out.println("x_"+i+","+t+" = "+model.getValue(x[i-1][t-1]));
                    countTasks += 1;
                    if(i <= 45){
                        countPrev += 1;
                    }else if(i <= 265){
                        countCorr += 1;
                    }else{
                        countMod +=1;
                    }
                // }else{
                //    System.out.println("x_"+i+","+t+" = 0"); 
                }
            }
        }*/
        /*
        System.out.println("Deterministic solution");
        System.out.println("Total: "+countTasks);
        System.out.println("Preventive: "+countPrev);
        System.out.println("Corrective: "+countCorr);
        System.out.println("Modifications: "+countMod);
        */
        for(int r = 1; r<= dp.getNResources(); r++){
            for(int n = 1; n<=dp.getNGroupedRes() ;n++){
                if(dp.getRNindicator(r, n)==true){
                    for(int t=1; t <= dp.getNWeeks(); t++){
                        if(model.getValue(k[r-1][n-1][t-1]) > 0){
                            //System.out.println("k_"+r+","+n+","+t+" = "+model.getValue(k[r-1][n-1][t-1]));
                        }
                    }
                }//else{
                   // for(int t=1; t <= dp.getNWeeks(); t++){
                     //   System.out.println("k_"+r+","+n+","+t+" = 0");
                    //}
                //}
            }
        }    
        
    }
    
    
    public double[][] getXsolution() throws IloException{
        double xOptimal[][] = new double[dp.getNBundles()][dp.getNWeeks()];
        for(int i=1; i <= dp.getNBundles(); i++){
            for(int t=1; t <= dp.getNWeeks(); t++){
                if(dp.getITindicator(i,t) == true){
                    xOptimal[i-1][t-1] = model.getValue(x[i-1][t-1]);
                    
                }
            }
        }
        return xOptimal;
    }
    
    
    //Release all objects
    public void end(){
        model.end();
    }  
}
