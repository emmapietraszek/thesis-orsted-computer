/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DeterministicModel;

/**
 *
 * @author XEPIE
 */
public class DeterministicProblem {
    
    private final int nTasks;
    private final int nBundles;
    private final int nWeeks;
    private final boolean ITindicator[][];
    private final int nResources;
    private final int nGroupedRes;
    private final boolean RNindicator[][];
    private final double values[][];
    private final int A[][];
    private final double capacities[][];
    private final double hours[][];
    
    
    public DeterministicProblem(int J,int Iset,int T, boolean IT[][], int R,int N,boolean RN[][],double W[][],int A[][],double C[][],double H[][]){       
        this.nTasks=J;
        this.nBundles=Iset;
        this.nWeeks=T;
        this.ITindicator=IT;
        this.nResources=R;
        this.nGroupedRes=N;
        this.RNindicator= RN;
        this.values=W;
        this.A=A;
        this.capacities=C;
        this.hours=H;        
    }

    public int getNTasks() {
        return nTasks;
    }

    public int getNBundles() {
        return nBundles;
    }

    public int getNWeeks() {
        return nWeeks;
    }
    
    public boolean[][] getITindicator(){
        return ITindicator;
    }

    public boolean getITindicator(int i, int t){
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return ITindicator[i-1][t-1];
    }
    
    public int getNResources() {
        return nResources;
    }

    public int getNGroupedRes() {
        return nGroupedRes;
    }
    
    public boolean[][] getRNindicator(){
        return RNindicator;
    }

    public boolean getRNindicator(int r,int n){
        if(r < 1 || r > nResources){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nResources+" ]");
        }
        if(n < 1 || n > nGroupedRes){
            throw new IllegalArgumentException("The resource group number must be in [ 1,"+nGroupedRes+" ]");
        }
        return RNindicator[r-1][n-1];
    }
        
    public double[][] getValues() {
        return values;
    }
    
    public double getValues(int i, int t) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return values[i-1][t-1];
    }

    public int[][] getA() {
        return A;
    }
    
    public int getA(int i, int j) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(j < 1 || j > nTasks){
            throw new IllegalArgumentException("The task number must be in [ 1,"+nTasks+" ]");
        }
        return A[i-1][j-1];
    }

    public double[][] getCapacities() {
        return capacities;
    }
    
    public double getCapacities(int n, int t) {
        if(n < 1 || n > nGroupedRes){
            throw new IllegalArgumentException("The resource group number must be in [ 1,"+nGroupedRes+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return capacities[n-1][t-1];
    }

    public double[][] getHours() {
        return hours;
    }
    
    public double getHours(int i, int r) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(r < 1 || r > nResources){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nResources+" ]");
        }
        return hours[i-1][r-1];
    }
        
    
}
