/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ScenarioGeneration;

import StochasticModel.StochasticProblem;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloQuadNumExpr;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author XEPIE
 */
public class ScenarioModel {
    
       
    private final IloCplex model;
    private final ScenarioProblem sp;
    private final IloIntVar x[];
    private final IloNumVar mu1;
    private final IloNumVar mu2;

    
    
    public ScenarioModel(ScenarioProblem sp) throws IloException{
        
        //Creating a object for the model and the problem
        this.model = new IloCplex();
        model.setParam(IloCplex.Param.MIP.Tolerances.AbsMIPGap, 1e-06);
        model.setParam(IloCplex.Param.MIP.Tolerances.MIPGap,1e-06);
        this.sp = sp;
 
        //Create desition variables
        x = new IloIntVar[sp.getnData()];
        
        //Initiaizing variables
        for(int d=1; d <= sp.getnData(); d++){
            x[d-1] = model.boolVar();            
        }
        mu1 = model.numVar(0, Double.POSITIVE_INFINITY);
        mu2 = model.numVar(0, Double.POSITIVE_INFINITY);
        
        //OBJECTIVE FUNCTION
        //Creating the liear part of the model
        IloLinearNumExpr lin = model.linearNumExpr();
        lin.addTerm(-2*sp.getw1(), mu1);
        lin.addTerm(-2*sp.getw2(), mu2);       
        //Creating the quadratic part of the model
        IloQuadNumExpr quad = model.quadNumExpr();
        quad.addTerm(1, mu1, mu1);
        quad.addTerm(1, mu2, mu2);
        //Adding all terms together
        model.addMinimize(
            model.sum(
                lin,
                quad
            )
        );
        
        //CONSTRAINTS
        IloLinearNumExpr lhs = model.linearNumExpr();
        for(int d=1; d <= sp.getnData(); d++){
            lhs.addTerm(1,x[d-1]);
        }
        model.addEq(lhs,sp.getnScenarios());
        
        IloLinearNumExpr rhs1 = model.linearNumExpr();
        for(int d=1; d <= sp.getnData(); d++){
            for(int t=1; t <= sp.getnWeeksMinusOne(); t++){
                rhs1.addTerm(sp.getData(d,t)/(sp.getnWeeksMinusOne()*sp.getnScenarios()), x[d-1]);
            }
        }
        model.addEq(mu1,rhs1);
        
        IloLinearNumExpr rhs2 = model.linearNumExpr();
        for(int d=1; d <= sp.getnData(); d++){
            for(int t=1; t <= sp.getnWeeksMinusOne(); t++){
                rhs2.addTerm(Math.pow(sp.getData(d,t), 2) /(sp.getnWeeksMinusOne()*sp.getnScenarios()), x[d-1]);
            }
        }
        model.addEq(mu2,rhs2);
        
        
     
    
    
    }
    
    
    
    public void solve() throws IloException{
        
        model.setOut(null);
        model.solve();
        double value1 = model.getObjValue() + Math.pow(sp.getw1(), 2) + Math.pow(sp.getw2(), 2);
        //System.out.println("Optimal objective value "+value1);
        //System.out.println("Optimal solution ");       
        for(int d=1; d <= sp.getnData(); d++){
            if(model.getValue(x[d-1])>0){
                //System.out.println("x_"+d+" = "+model.getValue(x[d-1]));
            }
        }
        //System.out.println("mu_1 = "+model.getValue(mu1));
        //System.out.println("mu_2 = "+model.getValue(mu2));
        
        //System.out.println("Scenarios");
        for(int d=1; d <= sp.getnData(); d++){
            if(model.getValue(x[d-1]) > 0){
                for(int t=1; t <= sp.getnWeeksMinusOne(); t++){
                    //System.out.println("data_"+d+","+t+" = "+sp.getData(d, t));
                }
            }
        }
        //System.out.println("Optimality gap: "+(model.getObjValue()-model.getBestObjValue()));
    }
    
    public double getObjective() throws IloException{
        return model.getObjValue() + Math.pow(sp.getw1(), 2) + Math.pow(sp.getw2(), 2);
    }
    
    
    public void generateCut(double optimalSolution[]) throws IloException{
        IloLinearIntExpr lhs = model.linearIntExpr();
        for(int d=1; d <= sp.getnData(); d++){
            if(optimalSolution[d-1] == 1){
                lhs.addTerm(x[d-1], 1);
            }else if(optimalSolution[d-1] == 0){
                lhs.addTerm(x[d-1], -1);
            }else{
                System.out.println("Solution is not binary (enough) for method adding cuts");
            }
        }
        IloRange cut = model.addLe(lhs, sp.getnScenarios() - 2);
        //System.out.println("Cut added: "+cut.toString());
    }
    
    public double[] getX() throws IloException{
        double X[] = new double[sp.getnData()];
        for(int d=1; d <= sp.getnData(); d++){
            X[d-1] = model.getValue(x[d-1]);
        }
        return X;
    }
    
    public double getOptimalityGap() throws IloException{
        return (model.getObjValue() - model.getBestObjValue());
    }
    
    
    
    //Release all objects
    public void end(){
        model.end();
    }
    
    
    
}
