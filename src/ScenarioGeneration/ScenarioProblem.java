/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ScenarioGeneration;

/**
 *
 * @author XEPIE
 */
public class ScenarioProblem {
    
    private final int nScenarios;
    private final int nData;
    private final int nWeeksMinusOne; 
    private final double data[][];
    private final double w1;
    private final double w2;
    
    public ScenarioProblem(int S,int D,int T,double[][] data,double w1,double w2){
        this.nScenarios = S;
        this.nData = D;
        this.nWeeksMinusOne = T;
        this.data = data;
        this.w1 = w1;
        this.w2 = w2;
    }

    public int getnScenarios() {
        return nScenarios;
    }

    public int getnData() {
        return nData;
    }

    public int getnWeeksMinusOne() {
        return nWeeksMinusOne;
    }

    public double[][] getData() {
        return data;
    }
      
    public double getData(int d, int t) {
        if(d < 1 || d > nData){
            throw new IllegalArgumentException("The data number must be in [ 1,"+nData+" ]");
        }
        if(t < 1 || t > nWeeksMinusOne){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeksMinusOne+" ]");
        }
        return data[d-1][t-1];
    }
      
    public double getw1() {
        return w1;
    }
    
    public double getw2() {
        return w2;
    }
    
    
    
}
