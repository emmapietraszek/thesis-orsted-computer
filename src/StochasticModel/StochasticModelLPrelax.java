/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StochasticModel;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author XEPIE
 */
public class StochasticModelLPrelax {
     
    private final IloCplex model;
    private final StochasticProblem sp;
    private final IloNumVar x[][];
    private final IloNumVar y[][][];
    private final IloNumVar z[][][];
    private final IloNumVar k[][][][];
  
        public StochasticModelLPrelax(StochasticProblem sp) throws IloException{
        
        //Creating a object for the model and the problem
        this.model = new IloCplex();
        this.sp = sp;
        
        //Create desition variables
        x = new IloNumVar[sp.getNBundles()][sp.getNWeeks()];
        y = new IloNumVar[sp.getNBundles()][sp.getNWeeks()][sp.getNScenarios()];
        z = new IloNumVar[sp.getNBundles()][sp.getNWeeks()][sp.getNScenarios()];
        k = new IloNumVar[sp.getNResources()][sp.getNGroupedRes()][sp.getNWeeks()][sp.getNScenarios()];
        
        //Initializing the variables
        for(int i=1; i <= sp.getNBundles(); i++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    x[i-1][t-1] = model.numVar(0,1);     
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        y[i-1][t-1][s-1] = model.numVar(0,1);
                        z[i-1][t-1][s-1] = model.numVar(0,1);
                    }
                }
            }
        }     
        for(int r=1; r <= sp.getNResources(); r++){
            for(int n=1; n <= sp.getNGroupedRes(); n++){
                if(sp.getRNindicator(r, n)==true){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        for(int s=1; s <= sp.getNScenarios(); s++){
                        k[r-1][n-1][t-1][s-1] = model.numVar(0, Double.POSITIVE_INFINITY);
                        }
                    }   
                }
            }        
        }
        
        //Objective funciton
        IloLinearNumExpr obj = model.linearNumExpr();
        for(int s=1; s <= sp.getNScenarios(); s++){
            for(int i=1; i <= sp.getNBundles(); i++){
                for(int t=1; t <= sp.getNWeeks(); t++){
                    if(sp.getITindicator(i, t)==true){
                        obj.addTerm(sp.getPi(s)*sp.getValues(i, t), y[i-1][t-1][s-1]);  
                        obj.addTerm(-sp.getPi(s)*sp.getPunishment(i), z[i-1][t-1][s-1]);
                    }
                }
            }
        }
        model.addMaximize(obj);
        
        // X-variable constraints
        for(int j=1; j <= sp.getNTasks(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i=1; i <= sp.getNBundles(); i++){
                for(int t=1; t <= sp.getNWeeks(); t++){
                    if(sp.getITindicator(i, t)==true){
                        lhs.addTerm(sp.getA(i, j), x[i-1][t-1]);
                    }
                }
            }
            model.addEq(lhs, 1);
        }

        //Y-variable constraints
        for(int j=1; j <= sp.getNTasks(); j++){
            for(int s=1; s <= sp.getNScenarios(); s++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int i=1; i <= sp.getNBundles(); i++){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        if(sp.getITindicator(i, t)==true){
                            lhs.addTerm(sp.getA(i, j), y[i-1][t-1][s-1]);
                        }
                    }
                }
                model.addEq(lhs, 1);
            }
        }
        for(int n=1; n <= sp.getNGroupedRes(); n++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                for(int s=1; s <= sp.getNScenarios(); s++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    for(int r=1; r <= sp.getNResources(); r++){
                        if(sp.getRNindicator(r, n)==true){
                            lhs.addTerm(k[r-1][n-1][t-1][s-1], 1);
                        }
                    }
                    model.addLe(lhs, sp.getCapacities(n,t,s));
                }
            }
        }
        for(int r=1; r <= sp.getNResources(); r++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                for(int s=1; s <= sp.getNScenarios(); s++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    for(int i=1; i <= sp.getNBundles(); i++){
                        if(sp.getITindicator(i, t)==true){
                            lhs.addTerm(sp.getHours(i,r), y[i-1][t-1][s-1]);
                        }
                    }
                    for(int n=1; n <= sp.getNGroupedRes(); n++){
                        if(sp.getRNindicator(r, n)==true){
                            lhs.addTerm(k[r-1][n-1][t-1][s-1], -1);
                        }
                    }
                    model.addLe(lhs, 0);
                }
            }
        }
        
        //Z-variable constraints
        for(int t=1; t <= sp.getNWeeks(); t++){
            for(int s=1; s <= sp.getNScenarios(); s++){
                for(int i=1; i <= sp.getNBundles(); i++){
                    if(sp.getITindicator(i, t)==true){
                        IloLinearNumExpr lhs = model.linearNumExpr();
                        lhs.addTerm(x[i-1][t-1],1);
                        lhs.addTerm(y[i-1][t-1][s-1], -1);
                        lhs.addTerm(z[i-1][t-1][s-1], -1);
                        model.addLe(lhs, 0);
                    }
                }
            }
        }  
    }
    
    //Solving method that prints the solution
    public void solve() throws IloException{        
        model.setOut(null);
        model.solve();
        System.out.println("Optimal objective value "+model.getObjValue());
        System.out.println("Optimal solution ");
        
        for(int i = 1; i<=sp.getNBundles(); i++){
            for(int t = 1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true && model.getValue(x[i-1][t-1]) > 0){
                    System.out.println("x_"+i+","+t+" = "+model.getValue(x[i-1][t-1]));
                //}else{
                //    System.out.println("x_"+i+","+t+" = 0");
                }
            }
        }
        for(int i = 1; i<=sp.getNBundles(); i++){
            for(int t = 1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        if(model.getValue(y[i-1][t-1][s-1]) > 0){
                            System.out.println("y_"+i+","+t+","+s+" = "+model.getValue(y[i-1][t-1][s-1]));
                        }
                    }
                //}else{
                //    for(int s=1; s <= sp.getNScenarios(); s++){
                //        System.out.println("y_"+i+","+t+","+s+" = 0");
                //    }
                }
            }
        }
        for(int i = 1; i<=sp.getNBundles(); i++){
            for(int t = 1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        if(model.getValue(z[i-1][t-1][s-1]) > 0){
                            System.out.println("z_"+i+","+t+","+s+" = "+model.getValue(z[i-1][t-1][s-1]));
                        }
                    }
                //}else{
                //    for(int s=1; s <= sp.getNScenarios(); s++){
                //        System.out.println("z_"+i+","+t+","+s+" = 0");
                //    }
                }
            }
        }        
        for(int r = 1; r<= sp.getNResources(); r++){
            for(int n = 1; n<=sp.getNGroupedRes() ;n++){
                if(sp.getRNindicator(r, n)==true){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        for(int s=1; s <= sp.getNScenarios(); s++){
                            if(model.getValue(k[r-1][n-1][t-1][s-1]) > 0){
                                System.out.println("k_"+r+","+n+","+t+","+s+" = "+model.getValue(k[r-1][n-1][t-1][s-1]));
                            }
                        }
                    }
                //}else{
                //    for(int t=1; t <= sp.getNWeeks(); t++){
                //        for(int s=1; s <= sp.getNScenarios(); s++){
                //            System.out.println("k_"+r+","+n+","+t+","+s+" = 0");
                //        }
                //    }
                }
            }       
        }        
    }
    
    //Release all objects
    public void end(){
        model.end();
    }
}