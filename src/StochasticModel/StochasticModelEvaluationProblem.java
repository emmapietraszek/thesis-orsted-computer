/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StochasticModel;

/**
 *
 * @author XEPIE
 */
public class StochasticModelEvaluationProblem {
    
           
    private final int nTasks;
    private final int nBundles;
    private final int nWeeks;
    private final boolean ITindicator[][];    
    private final int nResources;
    private final int nGroupedRes;
    private final boolean RNindicator[][];    
    private final int nScenarios;
    private final double values[][];
    private final double punishments[];
    private final int A[][];
    private final double capacities[][][];
    private final double hours[][];
    private final double pi[];
    private final double x[][];
    
    
    public StochasticModelEvaluationProblem(int J,int I,int T,boolean IT[][], int R,int N, boolean RN[][], int S,double W[][],double V[],int A[][],double C[][][],double H[][],double pi[],double x[][]){
        
        this.nTasks = J;
        this.nBundles = I;
        this.nWeeks = T;
        this.ITindicator = IT;
        this.nResources = R;
        this.nGroupedRes = N;
        this.RNindicator = RN;
        this.nScenarios = S;
        this.values = W;
        this.punishments = V;
        this.A = A;
        this.capacities = C;
        this.hours = H;        
        this.pi = pi;
        this.x = x;
    }
    
    //GETTERS
    public int getNTasks() {
        return nTasks;
    }

    public int getNBundles() {
        return nBundles;
    }

    public int getNWeeks() {
        return nWeeks;
    }
    
    public boolean[][] getITindicator(){
        return ITindicator;
    }

    public boolean getITindicator(int i, int t){
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return ITindicator[i-1][t-1];
    }
    
    public int getNResources() {
        return nResources;
    }

    public int getNGroupedRes() {
        return nGroupedRes;
    }
   
    public boolean[][] getRNindicator(){
        return RNindicator;
    }

    public boolean getRNindicator(int r,int n){
        if(r < 1 || r > nResources){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nResources+" ]");
        }
        if(n < 1 || n > nGroupedRes){
            throw new IllegalArgumentException("The resource group number must be in [ 1,"+nGroupedRes+" ]");
        }
        return RNindicator[r-1][n-1];
    }
    
    public int getNScenarios(){
        return nScenarios;
    }
    
    public double[][] getValues() {
        return values;
    }
    
    public double getValues(int i, int t) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return values[i-1][t-1];
    }

    public double[] getPunishments(){
        return punishments;
    }
    
    public double getPunishment(int i) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        return punishments[i-1];
    }
    
    public int[][] getA() {
        return A;
    }
    
    public int getA(int i, int j) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(j < 1 || j > nTasks){
            throw new IllegalArgumentException("The task number must be in [ 1,"+nTasks+" ]");
        }
        return A[i-1][j-1];
    }

    public double[][][] getCapacities() {
        return capacities;
    }
    
    public double getCapacities(int n, int t, int s) {
        if(n < 1 || n > nGroupedRes){
            throw new IllegalArgumentException("The resource group number must be in [ 1,"+nGroupedRes+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        if(s < 1 || s > nScenarios){
            throw new IllegalArgumentException("The scenario number must be in [ 1,"+nScenarios+" ]");
        }
        return capacities[n-1][t-1][s-1];
    }

    public double[][] getHours() {
        return hours;
    }
    
    public double getHours(int i, int t) {
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return hours[i-1][t-1];
    }
    
    public double[] getPi(){
        return pi;
    }
    
    public double getPi(int s){
        return pi[s-1];
    }

    public double[][] getX(){
        return x;
    }
    
    public double getX(int i,int t){
        if(i < 1 || i > nBundles){
            throw new IllegalArgumentException("The bundle number must be in [ 1,"+nBundles+" ]");
        }
        if(t < 1 || t > nWeeks){
            throw new IllegalArgumentException("The week number must be in [ 1,"+nWeeks+" ]");
        }
        return x[i-1][t-1];
    }
    
    
    
    
    
}