/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StochasticModel;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author XEPIE
 */
public class StochasticModelEvaluation {
    
       
    private final IloCplex model;
    private final StochasticModelEvaluationProblem sp;
    private final IloIntVar y[][][];
    private final IloIntVar z[][][];
    private final IloNumVar k[][][][];
    
    public StochasticModelEvaluation(StochasticModelEvaluationProblem sp) throws IloException{
    
       
        //Creating a object for the model and the problem
        this.model = new IloCplex();
        model.setParam(IloCplex.Param.MIP.Tolerances.MIPGap,5e-02);
        this.sp = sp;
        
        //Create desition variables
        y = new IloIntVar[sp.getNBundles()][sp.getNWeeks()][sp.getNScenarios()];
        z = new IloIntVar[sp.getNBundles()][sp.getNWeeks()][sp.getNScenarios()];
        k = new IloNumVar[sp.getNResources()][sp.getNGroupedRes()][sp.getNWeeks()][sp.getNScenarios()];
        
        //Initializing the variables
        for(int i=1; i <= sp.getNBundles(); i++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){   
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        y[i-1][t-1][s-1] = model.boolVar();
                        z[i-1][t-1][s-1] = model.boolVar();
                    }
                }
            }
        }     
        for(int r=1; r <= sp.getNResources(); r++){
            for(int n=1; n <= sp.getNGroupedRes(); n++){
                if(sp.getRNindicator(r, n)==true){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        for(int s=1; s <= sp.getNScenarios(); s++){
                            k[r-1][n-1][t-1][s-1] = model.numVar(0, Double.POSITIVE_INFINITY);
                        }
                    }   
                }
            }        
        }
               
        //Objective funciton
        IloLinearNumExpr obj = model.linearNumExpr();
        for(int s=1; s <= sp.getNScenarios(); s++){
            for(int i=1; i <= sp.getNBundles(); i++){
                for(int t=1; t <= sp.getNWeeks(); t++){
                    if(sp.getITindicator(i, t)==true){
                        obj.addTerm(sp.getPi(s)*sp.getValues(i, t), y[i-1][t-1][s-1]);  
                        obj.addTerm(-sp.getPi(s)*sp.getPunishment(i), z[i-1][t-1][s-1]);
                    }
                }
            }
        }
        //System.out.println(obj);
        model.addMaximize(obj);
                
        //Y-variable constraints
        for(int j=1; j <= sp.getNTasks(); j++){
            for(int s=1; s <= sp.getNScenarios(); s++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int i=1; i <= sp.getNBundles(); i++){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        if(sp.getITindicator(i, t)==true){
                            lhs.addTerm(sp.getA(i, j), y[i-1][t-1][s-1]);
                        }
                    }
                }
                model.addEq(lhs, 1);
            }
        }
        for(int n=1; n <= sp.getNGroupedRes(); n++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                for(int s=1; s <= sp.getNScenarios(); s++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    for(int r=1; r <= sp.getNResources(); r++){
                        if(sp.getRNindicator(r, n)==true){
                            lhs.addTerm(k[r-1][n-1][t-1][s-1], 1);
                        }
                    }
                    model.addLe(lhs, sp.getCapacities(n,t,s));
                }
            }
        }
        for(int r=1; r <= sp.getNResources(); r++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                for(int s=1; s <= sp.getNScenarios(); s++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    for(int i=1; i <= sp.getNBundles(); i++){
                        if(sp.getITindicator(i, t)==true){
                            lhs.addTerm(sp.getHours(i,r), y[i-1][t-1][s-1]);
                        }
                    }
                    for(int n=1; n <= sp.getNGroupedRes(); n++){
                        if(sp.getRNindicator(r, n)==true){
                            lhs.addTerm(k[r-1][n-1][t-1][s-1], -1);
                        }
                    }
                    model.addLe(lhs, 0);
                }
            }
        }
        
        //Z-variable constraints
        for(int t=1; t <= sp.getNWeeks(); t++){
            for(int s=1; s <= sp.getNScenarios(); s++){
                for(int i=1; i <= sp.getNBundles(); i++){
                    if(sp.getITindicator(i, t)==true){
                        IloLinearNumExpr lhs = model.linearNumExpr();
                        lhs.addTerm(y[i-1][t-1][s-1], 1);
                        lhs.addTerm(z[i-1][t-1][s-1], 1);
                        model.addGe(lhs,sp.getX(i, t));
                    }
                }
            }
        }
    
    
    }
    
    public void solve() throws IloException{       
        model.setOut(null);
        model.solve();
        //System.out.println("Optimal objective value "+model.getObjValue());
        //System.out.println("Optimality gap: "+(model.getObjValue()-model.getBestObjValue()));
        for(int i = 1; i<=sp.getNBundles(); i++){
            for(int t = 1; t < sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        if(model.getValue(y[i-1][t-1][s-1]) > 0){
                            //System.out.println("y_"+i+","+t+","+s+" = "+model.getValue(y[i-1][t-1][s-1]));
                        }
                    }
                }
            }
        }
        int countZ = 0;
        for(int i = 1; i<=sp.getNBundles(); i++){
            for(int t = 1; t < sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    for(int s=1; s <= sp.getNScenarios(); s++){
                        if(model.getValue(z[i-1][t-1][s-1]) > 0){
                            //System.out.println("z_"+i+","+t+","+s+" = "+model.getValue(z[i-1][t-1][s-1]));
                            countZ += 1;
                        }
                    }
                }
            }
        } 
        //System.out.println("Number of deviating bundles in the large tree: "+countZ);
    }
    
    public double getObj() throws IloException{
        return model.getObjValue();
    }
    
    public double getZsolution(int ii,int tt,int ss) throws IloException{
        double zOptimal[][][] = new double[sp.getNBundles()][sp.getNWeeks()][sp.getNScenarios()];
        for(int i=1; i <= sp.getNBundles(); i++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                for(int s=1; s <= sp.getNScenarios(); s++){
                    if(sp.getITindicator(i, t) == true){
                        zOptimal[i-1][t-1][s-1] = model.getValue(z[i-1][t-1][s-1]);
                    }
                }
            }
        }
        return zOptimal[ii-1][tt-1][ss-1];
    }
     
    
    
    //Release all objects
    public void end(){
        model.end();
    }
    
}
