/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StochasticModel;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author XEPIE
 */
public class EVPImodel {
        
    private final IloCplex model;
    private final StochasticProblem sp;
    private final IloIntVar x[][];
    private final IloIntVar y[][];
    private final IloIntVar z[][];
    private final IloNumVar k[][][];
    
    public EVPImodel(StochasticProblem sp, int scenarioNumber) throws IloException{
        
        //Creating a object for the model and the problem
        this.model = new IloCplex();
        //model.setParam(IloCplex.Param.MIP.Tolerances.AbsMIPGap, 1e-03);
        model.setParam(IloCplex.Param.MIP.Tolerances.MIPGap,5e-02);       
        
        this.sp = sp;
        
        //Create desition variables
        x = new IloIntVar[sp.getNBundles()][sp.getNWeeks()];
        y = new IloIntVar[sp.getNBundles()][sp.getNWeeks()];
        z = new IloIntVar[sp.getNBundles()][sp.getNWeeks()];
        k = new IloNumVar[sp.getNResources()][sp.getNGroupedRes()][sp.getNWeeks()];
        
        //Initializing the variables
        for(int i=1; i <= sp.getNBundles(); i++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    x[i-1][t-1] = model.boolVar();     
                    y[i-1][t-1] = model.boolVar();
                    z[i-1][t-1] = model.boolVar();
                }
            }
        }     
        for(int r=1; r <= sp.getNResources(); r++){
            for(int n=1; n <= sp.getNGroupedRes(); n++){
                if(sp.getRNindicator(r, n)==true){
                    for(int t=1; t <= sp.getNWeeks(); t++){
                        k[r-1][n-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);                      
                    }   
                }
            }        
        }
        
        //Objective funciton
        IloLinearNumExpr obj = model.linearNumExpr();
        for(int i=1; i <= sp.getNBundles(); i++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                if(sp.getITindicator(i, t)==true){
                    obj.addTerm(sp.getPi(scenarioNumber)*sp.getValues(i, t), y[i-1][t-1]);  
                    obj.addTerm(-sp.getPi(scenarioNumber)*sp.getPunishment(i), z[i-1][t-1]);
                }
            }
        }
        
        //System.out.println(obj);
        model.addMaximize(obj);
        
        // X-variable constraints
        for(int j=1; j <= sp.getNTasks(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i=1; i <= sp.getNBundles(); i++){
                for(int t=1; t <= sp.getNWeeks(); t++){
                    if(sp.getITindicator(i, t)==true){
                        lhs.addTerm(sp.getA(i, j), x[i-1][t-1]);
                    }
                }
            }
            model.addEq(lhs, 1);
        }

        //Y-variable constraints
        for(int j=1; j <= sp.getNTasks(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i=1; i <= sp.getNBundles(); i++){
                for(int t=1; t <= sp.getNWeeks(); t++){
                    if(sp.getITindicator(i, t)==true){
                        lhs.addTerm(sp.getA(i, j), y[i-1][t-1]);
                    }
                }
            }
            model.addEq(lhs, 1);            
        }
        for(int n=1; n <= sp.getNGroupedRes(); n++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int r=1; r <= sp.getNResources(); r++){
                    if(sp.getRNindicator(r, n)==true){
                        lhs.addTerm(k[r-1][n-1][t-1], 1);
                    }
                }
                model.addLe(lhs, sp.getCapacities(n,t,scenarioNumber));               
            }
        }
        for(int r=1; r <= sp.getNResources(); r++){
            for(int t=1; t <= sp.getNWeeks(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int i=1; i <= sp.getNBundles(); i++){
                    if(sp.getITindicator(i, t)==true){
                        lhs.addTerm(sp.getHours(i,r), y[i-1][t-1]);
                    }
                }
                for(int n=1; n <= sp.getNGroupedRes(); n++){
                    if(sp.getRNindicator(r, n)==true){
                        lhs.addTerm(k[r-1][n-1][t-1], -1);
                    }
                }
                model.addLe(lhs, 0);
            }        
        }
        
        //Z-variable constraints
        for(int t=1; t <= sp.getNWeeks(); t++){
            for(int i=1; i <= sp.getNBundles(); i++){
                if(sp.getITindicator(i, t)==true){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    lhs.addTerm(x[i-1][t-1],1);
                    lhs.addTerm(y[i-1][t-1], -1);
                    lhs.addTerm(z[i-1][t-1], -1);
                    model.addLe(lhs, 0);
                }
            }            
        }  
        
        
        
    }   
    
    public void solve() throws IloException{
        
        model.setOut(null);
        model.solve();
        
        //System.out.println("Optimal objective value "+model.getObjValue());
        
    }
            
    public double getObj() throws IloException{
        return model.getObjValue();
    }
    
    
    //Release all objects
    public void end(){
        model.end();
    }
    
    
}
