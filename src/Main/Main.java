/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import DeterministicModel.DeterministicModel;
import DeterministicModel.DeterministicProblem;
import ScenarioGeneration.ScenarioModel;
import ScenarioGeneration.ScenarioProblem;
import StochasticModel.EVPImodel;
import StochasticModel.StochasticModel;
import StochasticModel.StochasticModelEvaluation;
import StochasticModel.StochasticModelEvaluationProblem;
import StochasticModel.StochasticProblem;
import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;



/**
 *
 * @author XEPIE
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, IloException{
    
                
        int nTasks = 342;
        int nWeeks = 6+1;
        int nScenarios = 20;
        
        File weather = new File("DataVejrUger2.txt");
        Scanner ss = new Scanner(weather);
        
        int nObsWeather = 1885;        
        int yearWeather[] = new int[nObsWeather];
        int monthWeather[] = new int[nObsWeather];      
        int weekWeather[] = new int[nObsWeather];
        int nWorkDays[] = new int[nObsWeather];  
        int indexWeather[] = new int[nObsWeather];    
        
        ss.nextLine();
        for(int N=1; N <= nObsWeather; N++){
            yearWeather[N-1] = ss.nextInt();
            monthWeather[N-1] = ss.nextInt();          
            weekWeather[N-1] = ss.nextInt();
            nWorkDays[N-1] = ss.nextInt();
            indexWeather[N-1] = ss.nextInt();
        }
        
        File tasks = new File("TaskData4.txt");
        Scanner c = new Scanner(tasks);
        

        int ID[] = new int[nTasks];
        String typeGeneral[] = new String[nTasks];
        double hoursTask[] = new double[nTasks];
        String turbine[] = new String[nTasks];
        int startYear[] = new int[nTasks]; 
        int startMonth[] = new int[nTasks];
        int startDay[] = new int[nTasks];
        int startWeek[] = new int[nTasks];
        int startYearModified[] = new int[nTasks];
        String endYear[] = new String[nTasks];
        String endMonth[] = new String[nTasks];
        String endDay[] = new String[nTasks];
        String endWeekStr[] = new String[nTasks];
        String endYearModifiedStr[] = new String[nTasks];
        String resourceGeneral[] = new String[nTasks];
        String resourceSpecific[] = new String[nTasks];
        String priority[] = new String[nTasks];
        String typeSpecific[] = new String[nTasks];
        int createdYear[] = new int[nTasks];
        int createdMonth[] = new int[nTasks];
        int createdDay[] = new int[nTasks];
        int createdWeek[] = new int[nTasks];
        int createdYearModified[] = new int[nTasks];
        
        
        c.nextLine();
        for(int j=1; j <= nTasks ; j++){
            ID[j-1] = c.nextInt();
            typeGeneral[j-1] = c.next();
            hoursTask[j-1] = c.nextDouble();
            turbine[j-1] = c.next();
            startYear[j-1] = c.nextInt();
            startMonth[j-1] = c.nextInt();
            startDay[j-1] = c.nextInt();
            startWeek[j-1] = c.nextInt();
            startYearModified[j-1] = c.nextInt();
            endYear[j-1] = c.next();
            endMonth[j-1] = c.next();
            endDay[j-1] = c.next();
            endWeekStr[j-1] = c.next();
            endYearModifiedStr[j-1] = c.next();
            resourceGeneral[j-1] = c.next();
            resourceSpecific[j-1] = c.next();
            priority[j-1] = c.next();
            typeSpecific[j-1] = c.next();
            createdYear[j-1] = c.nextInt();
            createdMonth[j-1] = c.nextInt();
            createdDay[j-1] = c.nextInt();
            createdWeek[j-1] = c.nextInt();
            createdYearModified[j-1] = c.nextInt();
        }
        
        //DATA that connects weeks with months for the weeks in the period of the data for the tasks.
        File calendar = new File("Calendar.txt");
        Scanner b = new Scanner(calendar);
        int nCalendarWeeks = 23;
        int calendarMonth[] = new int[nCalendarWeeks];
        int calendarWeek[] = new int[nCalendarWeeks];
        int calendarYear[] = new int[nCalendarWeeks];
        
        b.nextLine();
        for(int w=1; w <= nCalendarWeeks; w++){
            calendarMonth[w-1] = b.nextInt();
            calendarWeek[w-1] = b.nextInt();
            calendarYear[w-1] = b.nextInt();
        }


        //First we find the number of turbines in the data.
        List<String> turbineNames = new ArrayList<String>(); //Creting list to hold the names of the turbines.
        turbineNames.add(turbine[0]); // Adding the first name to the list.
        List<String> years = new ArrayList<String>();
        years.add(endYear[0]);
        List<String> resourceNames = new ArrayList<String>();
        resourceNames.add(resourceGeneral[0]);
        List<String> priorityNames = new ArrayList<String>();
        priorityNames.add(priority[0]);
        
        for(int j=2; j <= nTasks; j++){
            if(turbineNames.contains(turbine[j-1]) == false){
                turbineNames.add(turbine[j-1]); //If the name is not already in the list, add it.
            }if(years.contains(endYear[j-1]) == false){
                years.add(endYear[j-1]);
            }if(resourceNames.contains(resourceGeneral[j-1]) == false){
                resourceNames.add(resourceGeneral[j-1]);
            }if(priorityNames.contains(priority[j-1]) == false){
                priorityNames.add(priority[j-1]);
            }
        }
        int nTurbines = turbineNames.size(); // The number of turbines is then the size of the list of names   
        int nResources = resourceNames.size(); // The number of resources is the size of the list of names
        priorityNames.remove("NULL");
        int nPriorities = priorityNames.size(); // The number of different priorities is the size of the list of names
        years.remove("NULL");

        //Assigning the length of the time-window for each type of priority-text
        int priorityNweeks[] = new int[nPriorities];
        for(int p=1; p <= nPriorities; p++){
            if(priorityNames.get(p-1).equals("WithinAyear")){
                priorityNweeks[p-1] = 51;   
            }else if(priorityNames.get(p-1).equals("WithinTwoMonths")){
                priorityNweeks[p-1] = 8;      
            }else if(priorityNames.get(p-1).equals("WithinTwoWeeks")){
                priorityNweeks[p-1] = 1;                       
            }else if(priorityNames.get(p-1).equals("WithinSixMonths")){
                priorityNweeks[p-1] = 25;                       
            }else if(priorityNames.get(p-1).equals("WithinAmonth")){
                priorityNweeks[p-1] = 4;                       
            }else if(priorityNames.get(p-1).equals("WithinThreeMonths")){
                priorityNweeks[p-1] = 12;                       
            }else if(priorityNames.get(p-1).equals("WithinAweek")){
                priorityNweeks[p-1] = 0;                       
            }else if(priorityNames.get(p-1).equals("Immediately")){
                priorityNweeks[p-1] = 0;                       
            }else if(priorityNames.get(p-1).equals("AtNextVisit")){
                priorityNweeks[p-1] = 12;  //A turbine is on average visited 4 times a year.
            }          
        }
        
       
        //Fixing missing end dates in data.
        int endWeek[] = new int[nTasks];
        int endYearModified[] = new int[nTasks];
        for(int j=1; j <= nTasks; j++){
            if(!endWeekStr[j-1].equals("#VALUE!")){
                endWeek[j-1] = Integer.parseInt(endWeekStr[j-1]);
                endYearModified[j-1] = Integer.parseInt(endYearModifiedStr[j-1]);
            }else{
                endWeek[j-1] = startWeek[j-1];
                endYearModified[j-1] = startYearModified[j-1];
            }
        }
        

        //Create the bundles (not done)
        int turbineNTasks[] = new int[nTurbines];
        
        for(int k=1; k <= nTurbines; k++){           
            for(int j=1; j <= nTasks; j++){
                if(turbineNames.get(k-1) == turbine[j-1]){
                    turbineNTasks[k-1] += 1;                
                }                
            } //SKAL LAVES OM
            //System.out.println(turbineNames.get(k-1)+" = "+turbineNTasks);
        }
        //Remember to assign hours to the bundles also. (by subtracting hours)
        
        
        
        // For now we only consider tasks
        int nBundles = nTasks;
                
        //Creating A
        int A[][] = new int[nBundles][nTasks];
        for(int i=1; i <= nBundles; i++){
            for(int j=1; j <= nTasks; j++){
                if(i <= nTasks & i == j){
                    A[i-1][j-1] = 1;
                }               
            }
        }
        //Remember to create A for the bundles.
        
        //Creating IT-indicator
        boolean ITindicator[][] = new boolean[nBundles][nWeeks];
        for(int i=1; i <= nBundles; i++){
            for(int t=1; t <= nWeeks; t++){
                ITindicator[i-1][t-1] = true;
            }
        }
        
        //Creating the RN-indicator
        int nGroupedRes = nResources + 1; // One extra grouped resource
        boolean RNindicator[][] = new boolean[nResources][nGroupedRes];
        for(int r=1; r <= nResources; r++){
            for(int n=1; n <= nGroupedRes; n++){
                if(r == n){ // All reources is an element of the group that only contains that resource.
                    RNindicator[r-1][n-1] = true;               
                }else if(resourceNames.get(r-1).equals("LICPREV") && n==4){ //Preventive and corrective work is in group 4.
                    RNindicator[r-1][n-1] = true;
                }else if(resourceNames.get(r-1).equals("LICCORR") && n==4){
                    RNindicator[r-1][n-1] = true;
                }else{
                    RNindicator[r-1][n-1] = false;
                }
            }
        }
        
        
        //Assigning hours to depend on resources
        double hours[][] = new double[nBundles][nResources];
        for(int i=1; i <= nTasks; i++){
            if(resourceGeneral[i-1].equals("LICEXT")){
                hours[i-1][0] = hoursTask[i-1];
                if(typeSpecific[i-1].equals("StatutoryModule")){
                    hours[i-1][0] = hoursTask[i-1]/2;
                    hours[i-1][1] = hoursTask[i-1]/2;
                }
            }else if(resourceGeneral[i-1].equals("LICPREV")){
                hours[i-1][1] = hoursTask[i-1];
            }else if(resourceGeneral[i-1].equals("LICCORR")){
                hours[i-1][2] = hoursTask[i-1];
            }
        }
        //ADD HOURS FOR BUNDLES!!!
        
        
        //CREATING WEEKS
        int currentWeek = 35;
        int currentYear = 2019;
        int currentMonth = 8;
        int weekNumbers[] = new int[nWeeks];
        int weekYears[] = new int[nWeeks];
        int weekMonths[] = new int[nWeeks];
        weekNumbers[0] = currentWeek;
        weekYears[0] = currentYear;
        weekMonths[0] = currentMonth;
        
        for(int t=2; t <= nWeeks; t++){
            if(currentWeek + (t-1) <= 52){
                weekNumbers[t-1] = currentWeek + (t-1);
                weekYears[t-1] = currentYear;
                for(int w=1; w <= nCalendarWeeks; w++){
                    if(weekNumbers[t-1] == calendarWeek[w-1] && weekYears[t-1] == calendarYear[w-1]){
                        weekMonths[t-1] = calendarMonth[w-1];
                    }
                }
            }else{
                weekNumbers[t-1] = currentWeek + (t-1) - 52;
                weekYears[t-1] = currentYear + 1;
                for(int w=1; w <= nCalendarWeeks; w++){
                    if(weekNumbers[t-1] == calendarWeek[w-1] && weekYears[t-1] == calendarYear[w-1]){
                        weekMonths[t-1] = calendarMonth[w-1];
                    }
                }
            }    
        }
     
        //ADD CALCULATIONS OF THE PUNISHMENTS
      
        
        //CREATING VALUE-FUNCTIONS
        double avgLoss[] = new double[]{196.5,120.3,57.9,30.3,19.3,11.5,9.0,14.2,25.2,71.4,107.8,142.9};
        double avgPower[] = new double[]{2.38,2.18,1.87,1.52,1.36,1.11,1.01,1.16,1.46,1.97,2.17,2.29};
        double propBreak = Double.valueOf(4)/Double.valueOf(52);
        double values[][] = new double[nBundles][nWeeks];
        double punishments[] = new double[nTasks];
        int midWeek;
        double count = 0;
        double nTasksInHorizon = 0;
        double totalHours[] = new double[nResources];
        
        for(int i=1; i <= nBundles; i++){
            if(i <= nTasks){
                if(typeSpecific[i-1].equals("StatutoryModule") || typeSpecific[i-1].equals("Skyman")){
                    //Punishments
                    if(typeSpecific[i-1].equals("StatutoryModule")){
                        hours[i-1][0] = hoursTask[i-1]/2;
                        hours[i-1][1] = hoursTask[i-1]/2;
                    }
                    punishments[i-1] = 15; //15
                    
                    //Hard constraints on tasks that are outside the timehorizon of the model
                    if((endWeek[i-1] < currentWeek && endYearModified[i-1] == currentYear) || endYearModified[i-1] < currentYear ||
                        (startWeek[i-1] >= weekNumbers[nWeeks-1] && startYearModified[i-1] == weekYears[nWeeks-1]) || startYearModified[i-1] > weekYears[nWeeks-1]){
                        for(int t=1; t < nWeeks; t++){ //If the task has a time-window outside the timehorizon of the model, the variables are set to zero.
                            ITindicator[i-1][t-1] = false;
                        }
                        values[i-1][nWeeks-1] = 0.0; //Value of scheduling in the extra week is zero.
                    }else{ //Value-function
                        nTasksInHorizon += 1;
                        //System.out.println(ID[i-1]);
                        for(int r=1; r <= nResources; r++){
                            totalHours[r-1] += hours[i-1][r-1];
                            //System.out.println("TotalHours_"+r+" = "+totalHours[r-1]);
                        }  
                        
                        for(int t=1; t <= nWeeks; t++){                                                                              
                            if(startYearModified[i-1] == endYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == startYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == endYearModified[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                weekYears[t-1] < endYearModified[i-1] && weekYears[t-1] > startYearModified[i-1]){
                                values[i-1][t-1] = 0.0;  
                            }else{
                                for(int w=1; w < nWeeks; w++){ //CHANGE WHEN THERE IS A PERIOD OVER A YEARCHANGE
                                    if(weekNumbers[t-1] == endWeek[i-1] + w){
                                        values[i-1][t-1] = - propBreak*avgLoss[weekMonths[t-1]-1];
                                        if(w > 1){
                                            for(int v=1; v <= w-1; v++){
                                                count = 0;
                                                for(int u=1; u <= v; u++){
                                                    count += avgPower[weekMonths[t-u-1]-1]*24*7;
                                                }
                                                values[i-1][t-1] += - propBreak*(count + avgLoss[weekMonths[t-1]-1]);
                                            }
                                        }
                                    }else if(weekNumbers[t-1] == startWeek[i-1] - w){
                                        values[i-1][t-1] = - propBreak*avgLoss[weekMonths[t-1]-1];
                                        if(w > 1){
                                            for(int v=1; v <= w-1; v++){
                                                count = 0;
                                                for(int u=1; u <= v; u++){
                                                    count += avgPower[weekMonths[t+u-1]-1]*24*7;
                                                }
                                                values[i-1][t-1] += - propBreak*(count + avgLoss[weekMonths[t-1]-1]);
                                            }
                                        }
                                    }
                                }                        
                            } 
                            //System.out.println("W_"+i+","+t+" = "+values[i-1][t-1]);
                        }
                    } //ADD A DIFFERENT VALUE FOR WEEK 7 HERE IF NEEDED                    
                }else if(typeSpecific[i-1].equals("ServiceModule") || typeSpecific[i-1].equals("Cranes")){
                    //Sets the punishment for rescheduling service modules and crane tasks.
                    punishments[i-1] = 10;   // 10
                    //Calculates the week between the start and end week.
                    if(endYearModified[i-1] == startYearModified[i-1]){
                        midWeek = startWeek[i-1] + (endWeek[i-1]-startWeek[i-1])/2;                       
                    }else{
                        midWeek = startWeek[i-1] + (endWeek[i-1]+52 - startWeek[i-1])/2;
                    }                   
                    //Create objects to hold the years
                    int year1 = startYearModified[i-1];
                    int year2 = endYearModified[i-1];
                    //New start date
                    if(midWeek - 2 > 0){
                        if(midWeek - 2 <= 52){
                            startWeek[i-1] = midWeek - 2;
                        }else{
                            startWeek[i-1] = midWeek - 2 - 52;
                            startYearModified[i-1] = year1 + 1;
                        }
                    }else{
                        startWeek[i-1] = midWeek - 2 + 52;
                        startYearModified[i-1] = year2 - 1;                        
                    }
                    //New end date
                    if(midWeek + 2 <= 52){
                        endWeek[i-1] = midWeek + 2;
                    }else{
                        endWeek[i-1] = midWeek + 2 - 52;
                        endYearModified[i-1] = year1 + 1;
                    }
                    
                    if( endWeek[i-1] < currentWeek && endYearModified[i-1] == currentYear || 
                        endYearModified[i-1] < currentYear ||
                        startWeek[i-1] >= weekNumbers[nWeeks-1] && startYearModified[i-1] == weekYears[nWeeks-1] || 
                        startYearModified[i-1] > weekYears[nWeeks-1]){
                        for(int t=1; t < nWeeks; t++){
                            ITindicator[i-1][t-1] = false;
                        }
                        values[i-1][nWeeks-1] = 0;
                    }else{
                        nTasksInHorizon += 1;
                        //System.out.println(ID[i-1]);
                        for(int r=1; r <= nResources; r++){
                            totalHours[r-1] += hours[i-1][r-1];
                        }  
                        for(int t=1; t <= nWeeks; t++){
                            if(startYearModified[i-1] == endYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == startYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == endYearModified[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                weekYears[t-1] < endYearModified[i-1] && weekYears[t-1] > startYearModified[i-1]){
                                values[i-1][t-1] = 0;
                            }else{
                                for(int w=1; w < nWeeks; w++){ //Does NOT contain year change
                                    if(weekNumbers[t-1] == endWeek[i-1] + w){
                                        values[i-1][t-1] = - propBreak*0.5*avgLoss[weekMonths[t-1]-1]; // Cost of a breakdown in the week where the task is scheduled.
                                        if(w > 1){
                                            for(int v=1; v <= w-1; v++){ //Loop over the diffent weeks that the turbine can break down.
                                                count = 0;
                                                for(int u=1; u <= v; u++){ //Loop that sums the breakdown costs of a breakdown in a particular week.
                                                    count += avgPower[weekMonths[t-u-1]-1]*24*7;                                                            
                                                }                                                
                                                values[i-1][t-1] += - propBreak*0.5*(count + avgLoss[weekMonths[t-1]-1]);
                                            }                                        
                                        }
                                    }else if(weekNumbers[t-1] == startWeek[i-1] - w){
                                        values[i-1][t-1] = - propBreak*0.5*avgLoss[weekMonths[t-1]-1];
                                        if(w > 1){
                                            for(int v=1; v <= w-1; v++){
                                                count = 0;
                                                for(int u=1; u <= v; u++){
                                                    count += avgPower[weekMonths[t+u-1]-1]*24*7;
                                                }
                                                values[i-1][t-1] += - propBreak*0.5*(count + avgLoss[weekMonths[t-1]-1]);
                                            }                                       
                                        }
                                    }
                                }
                            }
                            //System.out.println("W_"+i+","+t+" = "+values[i-1][t-1]);
                        }                   
                    }               
                }else if(typeSpecific[i-1].equals("CMM") || typeSpecific[i-1].equals("ZR02")){ //SOME ENDWEEKS DOES NOT EXIST           
                    punishments[i-1] = 200; //200
                    if(!priority[i-1].equals("NULL")){ //If the task has a priority, the time-window is changed if the priority is earlier.                        
                        //System.out.println("Old end week = "+endWeek[i-1]);
                        //System.out.println("Old end Year = "+endYearModified[i-1]);
                        for(int p=1; p <= nPriorities; p++){
                            if(priority[i-1].equals(priorityNames.get(p-1))){
                                if(createdWeek[i-1] + priorityNweeks[p-1] < endWeek[i-1] && createdYear[i-1] == endYearModified[i-1]){                                   
                                    endWeek[i-1] = createdWeek[i-1] + priorityNweeks[p-1]; //The end week is changed to the priority week, as it is earlier. Year is unchanged.
                                }else if(createdWeek[i-1] + priorityNweeks[p-1] <= 52 && createdYear[i-1] < endYearModified[i-1]){
                                    endWeek[i-1] = createdWeek[i-1] + priorityNweeks[p-1]; //If the priority date is in an earlier year, then both week and year is changed.
                                    endYearModified[i-1] = createdYear[i-1];
                                    //System.out.println("created = "+createdWeek[i-1]+", priority = "+priorityNweeks[p-1]);
                                }else if(createdWeek[i-1] + priorityNweeks[p-1] > 52){ //Looking at the case where the weeks goes into the next year.
                                    if(createdYear[i-1] + 1 < endYearModified[i-1]){
                                        endWeek[i-1] = createdWeek[i-1] + priorityNweeks[p-1] - 52;
                                        endYearModified[i-1] = createdYear[i-1] + 1;
                                        //System.out.println("0");
                                    }else if(createdWeek[i-1] + priorityNweeks[p-1] - 52 <= endWeek[i-1] && createdYear[i-1] + 1 == endYearModified[i-1]){
                                        endWeek[i-1] = createdWeek[i-1] + priorityNweeks[p-1] - 52;
                                        //System.out.println("1");
                                    }
                                }
                            }
                        }
                        //System.out.println("New end week = "+endWeek[i-1]);
                        //System.out.println("New end year = "+endYearModified[i-1]);                       
                    }                 
                    if((endWeek[i-1] < currentWeek && endYearModified[i-1] == currentYear) || endYearModified[i-1] < currentYear){
                        for(int t=1; t < nWeeks; t++){
                            ITindicator[i-1][t-1] = false;
                        }
                        values[i-1][nWeeks-1] = 0;
                    }else{
                        nTasksInHorizon += 1;
                        //System.out.println(ID[i-1]);
                        for(int r=1; r <= nResources; r++){
                            totalHours[r-1] += hours[i-1][r-1];
                        }  
                        for(int t=1; t <= nWeeks; t++){
                            if(weekYears[t-1] == endYearModified[i-1] && weekNumbers[t-1] <= endWeek[i-1] || weekYears[t-1] < endYearModified[i-1]){
                                if(typeSpecific[i-1].equals("CMM")){
                                    values[i-1][t-1] = 0;
                                }else{
                                    values[i-1][t-1] = 0.1;
                                }
                            }else{
                                for(int w=1; w < nWeeks; w++){
                                    if(weekNumbers[t-1] == endWeek[i-1] + w){
                                        values[i-1][t-1] = - avgLoss[weekMonths[t-1]-1];
                                        if(w > 1){
                                            count = 0;
                                            for(int u=1; u <= w-1; u++){
                                                count += avgPower[weekMonths[t-u-1]-1]*24*7;
                                            }
                                            values[i-1][t-1] += - count;                                             
                                        }                                        
                                    }
                                }                             
                            }
                            //System.out.println("W_"+i+","+t+" = "+values[i-1][t-1]);
                        }      
                    }            
                }else if(typeSpecific[i-1].equals("ZR03")){ 
                    punishments[i-1] = 0.03; //0.03
                    if(endWeek[i-1] < currentWeek && endYearModified[i-1] == currentYear || 
                        startWeek[i-1] >= weekNumbers[nWeeks-1] && startYearModified[i-1] == weekYears[nWeeks-1] || 
                        endYearModified[i-1] < currentYear ||
                        startYearModified[i-1] > weekYears[nWeeks-1]){
                        for(int t=1; t < nWeeks; t++){
                            ITindicator[i-1][t-1] = false;
                        }
                        values[i-1][nWeeks-1] = 0;
                    }else{
                        nTasksInHorizon += 1;
                        //System.out.println(ID[i-1]);
                        for(int r=1; r <= nResources; r++){
                            totalHours[r-1] += hours[i-1][r-1];
                        }  
                        for(int t=1; t < nWeeks; t++){                                                                              
                            if(startYearModified[i-1] == endYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == startYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == endYearModified[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                weekYears[t-1] < endYearModified[i-1] && weekYears[t-1] > startYearModified[i-1]){                               
                                values[i-1][t-1] = 0.3;                                
                            }else{
                                values[i-1][t-1] = 0;                  
                            } 
                            //System.out.println("W_"+i+","+t+" = "+values[i-1][t-1]);
                        }
                        values[i-1][nWeeks-1]=0;
                    }               
                }else if(typeSpecific[i-1].equals("ZR04")){
                    punishments[i-1] = 0.02; //0.02
                    if(endWeek[i-1] < currentWeek && endYearModified[i-1] == currentYear || 
                        startWeek[i-1] >= weekNumbers[nWeeks-1] && startYearModified[i-1] == weekYears[nWeeks-1] || 
                        endYearModified[i-1] < currentYear ||
                        startYearModified[i-1] > weekYears[nWeeks-1]){
                        for(int t=1; t < nWeeks; t++){
                            ITindicator[i-1][t-1] = false;                            
                        }
                        values[i-1][nWeeks-1] = 0;
                    }else{
                        nTasksInHorizon += 1;
                        //System.out.println(ID[i-1]);
                        for(int r=1; r <= nResources; r++){
                            totalHours[r-1] += hours[i-1][r-1];
                        }  
                        for(int t=1; t < nWeeks; t++){                                                                              
                            if(startYearModified[i-1] == endYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == startYearModified[i-1] && weekNumbers[t-1] >= startWeek[i-1] ||
                                startYearModified[i-1] < endYearModified[i-1] && weekYears[t-1] == endYearModified[i-1] && weekNumbers[t-1] <= endWeek[i-1] ||
                                weekYears[t-1] < endYearModified[i-1] && weekYears[t-1] > startYearModified[i-1]){
                                values[i-1][t-1] = 0.2;
                            }else{
                                values[i-1][t-1] = 0;                  
                            } 
                        }
                        values[i-1][nWeeks-1] = 0;
                    }               
                }
             
            }
        }
        /*
        System.out.println("Number of tasks in the time horizon: "+nTasksInHorizon);
        for(int r=1; r <= nResources; r++){
            System.out.println("Number of total hours needed of resource "+r+" = "+totalHours[r-1]);
        }
        */
        
        //CAPACITIES  
        double capacitiesFull[][] = new double[nGroupedRes][nWeeks-1]; //The capacity when all days are acsess days.
        for(int t=1; t < nWeeks; t++){
            capacitiesFull[2][t-1] = 35; 
            capacitiesFull[3][t-1] = 35; //+110
            capacitiesFull[0][t-1] = 25;
        }
        //capacitiesFull[0][0] = 30;
        //capacitiesFull[0][1] = 30;
        //capacitiesFull[0][3] = 30;
        //When the service modules are in the horizon, the capacities for preventive must be higher in some weeks.
        
        
        
              
        //SCENARIOS
        double pi[] = new double[nScenarios];
        for(int s=1; s <= nScenarios; s++){
            pi[s-1] = 1/Double.valueOf(nScenarios);
        }
        
        int currentWeekScenarios = 3; // 3, 29, 46
        int nWeeksMinusOne = nWeeks - 1;
        int weekInterval = 2;
        int nScenarioTrees = 10;
        double nAccessDays[][][] = new double[nScenarios][nWeeksMinusOne][nScenarioTrees];
        
        //double sum1 = 0;
        //double sum2 = 0;
        //double sum3 = 0;
                
        System.out.println("Week "+currentWeekScenarios);
        List<Integer> IDofCloseWeeks = new ArrayList<Integer>();
        for(int w=1; w <= nObsWeather - nWeeksMinusOne + 1; w++){
            if(weekWeather[w-1] >= currentWeekScenarios - weekInterval && weekWeather[w-1] <= currentWeekScenarios + weekInterval){
                IDofCloseWeeks.add(indexWeather[w-1]);
            }            
        }
        int nData = IDofCloseWeeks.size();
        System.out.println("The number D = "+nData);
        //System.out.println(IDofCloseWeeks);
        
        double data[][] = new double[nData][nWeeksMinusOne];
        for(int d=1; d <= nData; d++){
            data[d-1][0] = nWorkDays[IDofCloseWeeks.get(d-1)-1];
            //System.out.println("data_"+d+",1 = "+data[d-1][0]);
            for(int t=2; t <= nWeeksMinusOne ; t++){
                data[d-1][t-1] = nWorkDays[IDofCloseWeeks.get(d-1)-1+(t-1)]; 
                //System.out.println("data_"+d+","+t+" = "+data[d-1][t-1]);
            }
        }
        
        //Calculating the target values of mean and second moment
        double w1 = 0;
        double w2 = 0;
        for(int d=1; d <= nData; d++){
            for(int t=1; t <= nWeeksMinusOne; t++){
                w1 += data[d-1][t-1]/(nData*nWeeksMinusOne);
                w2 += Math.pow(data[d-1][t-1],2)/(nData*nWeeksMinusOne);
            }        
        }
        //System.out.println("w1 = "+w1);
        //System.out.println("w2 = "+w2);
        
        ScenarioProblem sgp = new ScenarioProblem(nScenarios,nData,nWeeksMinusOne,data,w1,w2);
        ScenarioModel sgm = new ScenarioModel(sgp);
             
        /*
        for(int s=1; s <= nScenarios; s++){
            for(int t=1; t <= nWeeksMinusOne; t++){
                nAccessDays[s-1][t-1][0] = sgm.getOptScenarios()[s-1][t-1];
                System.out.println("Ndays_"+s+","+t+" = "+nAccessDays[s-1][t-1]);
            }
        }
        */
        //System.out.println("Run time: "+(end1-start1)/1000F);
        //sum1 += (end1-start1)/1000F;
        //sum2 += sgm.getObjective();
        //sum3 += sgm.getOptimalityGap();
      
        for(int n=1; n <= nScenarioTrees; n++){           
            long start2 = System.currentTimeMillis();
            sgm.solve();
            long end2 = System.currentTimeMillis();
            
            double optSolution[] = sgm.getX();     
            int countScenarioIndex = 0;
            //System.out.println("Tree "+n);
            for(int d=1; d <= nData; d++){
                if(optSolution[d-1]==1){
                    for(int t=1; t <= nWeeksMinusOne; t++){
                        nAccessDays[countScenarioIndex][t-1][n-1] = data[d-1][t-1]; 
                        //System.out.println("Scenario_"+(countScenarioIndex+1)+","+t+" = "+nAccessDays[countScenarioIndex][t-1][n-1]);
                    }
                    countScenarioIndex += 1;
                }           
            }           
            sgm.generateCut(optSolution);         
            //sum1 += (end2-start2)/1000F;
            //sum2 += sgm.getObjective();
            //sum3 += sgm.getOptimalityGap();
        }
        
        //System.out.println("Avg. objective value: "+sum2/nDifferentScenarios);
        //System.out.println("Avg. run time: "+sum1/nDifferentScenarios);
        //System.out.println("Avg. optimality gap: "+sum3/nDifferentScenarios);
        
        
        double capFullParam = 1;
        for(int h=1; h <= 2; h++){
            System.out.println("Factor: "+capFullParam);
        //Creating the capacity trees
        double capacityTrees[][][][] = new double[nGroupedRes][nWeeks][nScenarios][nScenarioTrees];
        for(int tree=1; tree <= nScenarioTrees; tree++){
            for(int n=1; n <= nGroupedRes; n++){
                for(int t=1; t < nWeeks; t++){
                    for(int s=1; s <= nScenarios; s++){
                        capacityTrees[n-1][t-1][s-1][tree-1] = (nAccessDays[s-1][t-1][tree-1]/7)*(capFullParam*capacitiesFull[n-1][t-1]);
                    }
                }
                for(int s=1; s <= nScenarios; s++){
                    capacityTrees[n-1][nWeeks-1][s-1][tree-1] = Double.POSITIVE_INFINITY;
                }
            }       
        }
        //Creating a large tree with all the data
        double capacityAllData[][][] = new double[nGroupedRes][nWeeks][nData];
        for(int n=1; n <= nGroupedRes; n++){
            for(int t=1; t < nWeeks; t++){
                for(int d=1; d <= nData; d++){
                    capacityAllData[n-1][t-1][d-1] = (data[d-1][t-1]/7)*(capFullParam*capacitiesFull[n-1][t-1]);
                }
            }
            for(int d=1; d <= nData; d++){
                capacityAllData[n-1][nWeeks-1][d-1] = Double.POSITIVE_INFINITY;
            }
        }
        double piD[] = new double[nData];
        for(int d=1; d <= nData; d++){
            piD[d-1] = 1/Double.valueOf(nData);
            //System.out.println(piD[d-1]);
        }
        
        
        //Calculate deterministic capacities and solve
        double capacitiesDet[][] = new double[nGroupedRes][nWeeks];
        double buffer = 0.1;
        //for(int bb=1; bb <= 10; bb++){ //when testing for the buffer
        //System.out.println("Buffer: "+buffer);
        for(int n=1; n <= nGroupedRes; n++){ //The capacity in the last week is infinitely large.
            capacitiesDet[n-1][nWeeks-1] = Double.POSITIVE_INFINITY;
        }
        for(int n=1; n <= nGroupedRes; n++){
            for(int t=1; t < nWeeks; t++){
                capacitiesDet[n-1][t-1] = (w1/7)*(capFullParam*capacitiesFull[n-1][t-1]); 
            }
        }
        
        //System.out.println("Deterministic problem");
        
        DeterministicProblem dp = new DeterministicProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,values,A,capacitiesDet,hours);
        DeterministicModel dm = new DeterministicModel(dp);
        dm.solve();
        
        
        
        int run = 0;
        double runTimes[] = new double[nScenarioTrees];
        double runTimesSum = 0;
        double capScenarios[][][] = new double[nGroupedRes][nWeeks][nScenarios];
        double average1 = 0;
        double max1 = Double.NEGATIVE_INFINITY;
        double min1 = Double.POSITIVE_INFINITY;
        double average2 = 0;
        double max2 = Double.NEGATIVE_INFINITY;
        double min2 = Double.POSITIVE_INFINITY;
        double average3 = 0;
        double max3 = Double.NEGATIVE_INFINITY;
        double min3 = Double.POSITIVE_INFINITY;
        double average4 = 0;
        double max4 = Double.NEGATIVE_INFINITY;
        double min4 = Double.POSITIVE_INFINITY;
        double average5 = 0;
        double average6 = 0;
        double average7 = 0;
        double average8 = 0;
        double average9 = 0;
        double average10 = 0;
        //double solutions[][][] = new double[nBundles][nWeeks][nScenarioTrees];
        //Finding optimal solutions
        while(run < nScenarioTrees){
            System.out.println("Run number "+(run+1));   
            //if(run >= 1){
            for(int n=1; n <= nGroupedRes; n++){
                for(int t=1; t <= nWeeks; t++){
                    for(int s=1; s <= nScenarios; s++){
                        capScenarios[n-1][t-1][s-1] = capacityTrees[n-1][t-1][s-1][run];
                    }
                }
            }
        //if(bb == 1){
            //Solving the stochastic model to get the optimal solution of each treee and check for in-sample-stability
            System.out.println("Optimal stochastic solution");
            StochasticProblem sp = new StochasticProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nScenarios,values,punishments,A,capScenarios,hours,pi);
            StochasticModel sm = new StochasticModel(sp);
            //System.out.println(" ");
            //System.out.println("The solution from tree "+(run+1));
            //long start = System.currentTimeMillis();
            sm.solve();
            //long end = System.currentTimeMillis();
            //System.out.println("Run time: "+(end-start)/1000F);
            //runTimesSum += (end-start)/1000F;
            
            int totalTasks2 = 0;
            int nPrev2 = 0;
            int nCorr2 = 0;
            int nMod2 =0;
            for(int i=1; i <= nBundles; i++){
                for(int t=1; t <= nWeeks; t++){
                    for(int s=1; s <= nScenarios; s++){
                        if(sm.getZsolution(i, t, s)==1 ){
                            if(ID[i-1] <= 45){
                                nPrev2 += 1;
                                totalTasks2 += 1;
                            }else if(ID[i-1] <= 265 && endWeek[i-1] <= (currentWeek + 5) && currentYear == endYearModified[i-1]){
                                nCorr2 += 1;
                                totalTasks2 += 1;
                            }else if(ID[i-1] > 265 && (values[i-1][t-1]>0)){
                                nMod2 += 1;
                                totalTasks2 += 1;
                            }
                        }
                    }
                }
            }
            average7 += totalTasks2;
            average8 += nPrev2;
            average9 += nCorr2;
            average10 += nMod2;
            
                 
            
            
          /*  for buffer
            average2 += sm.getObj();
            if(max2 < sm.getObj()){
                max2 = sm.getObj();
            }
            if(min2 > sm.getObj()){
                min2 = sm.getObj();
            }
            
            if(run == nScenarioTrees -1){
            System.out.println("Optimal stochastic solution");
            System.out.println("Average: "+average2/nScenarioTrees);
            System.out.println("Max: "+max2);
            System.out.println("Min: "+min2);
        */
        //    }
        //}
            /*
            System.out.println("Evaluated in all data");
            StochasticModelEvaluationProblem spLargeTree = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,sm.getXsolution());
            StochasticModelEvaluation smeLargeTree = new StochasticModelEvaluation(spLargeTree);
            //StochasticProblem spLargeTree = new StochasticProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD);
            //StochasticModelEvaluation2 smeLargeTree = new StochasticModelEvaluation2(spLargeTree,thisSolution);
            smeLargeTree.solve();
            */
            //}
            //System.out.println(" ");
            //run += 1;
        //}    
        /*
        System.out.println("Number of scenarios: "+nScenarios);
        System.out.println("Number of runs: "+nScenarioTrees);
        System.out.print("Average run time: "+runTimesSum/nScenarioTrees);
        */
        //System.out.print(" ");
        
            
        
            //EEV in small tree
            System.out.println("Value in small tree");
            StochasticModelEvaluationProblem eevSmall = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nScenarios,values,punishments,A,capScenarios,hours,pi,dm.getXsolution());
            StochasticModelEvaluation eevmSmall = new StochasticModelEvaluation(eevSmall);
            eevmSmall.solve();
        
            int totalTasks1 = 0;
            int nPrev1 = 0;
            int nCorr1 = 0;
            int nMod1 =0;
            for(int i=1; i <= nBundles; i++){
                for(int t=1; t <= nWeeks; t++){
                    for(int s=1; s <= nScenarios; s++){
                        if(eevmSmall.getZsolution(i, t, s)==1 ){
                            if(ID[i-1] <= 45){
                                nPrev1 += 1;
                                totalTasks1 += 1;
                            }else if(ID[i-1] <= 265 && endWeek[i-1] <= (currentWeek + 5) && currentYear == endYearModified[i-1]){
                                nCorr1 += 1;
                                totalTasks1 += 1;
                            }else if(ID[i-1] > 265 && (values[i-1][t-1]>0)){
                                nMod1 += 1;
                                totalTasks1 += 1;
                            }
                        }
                    }
                }
            }
            average3 += totalTasks1;
            average4 += nPrev1;
            average5 += nCorr1;
            average6 += nMod1;
            
            
            
            //VSS in small tree
            System.out.println("VSS from small tree = "+(sm.getObj() - eevmSmall.getObj()));       
            average1 += sm.getObj() - eevmSmall.getObj();
            if(max1 < sm.getObj() - eevmSmall.getObj()){
                max1 = sm.getObj() - eevmSmall.getObj();
            }
            if(min1 > sm.getObj() - eevmSmall.getObj()){
                min1 = sm.getObj() - eevmSmall.getObj();
            }
            /*
            //EEV in large tree
            System.out.println("Value in large tree");
            StochasticModelEvaluationProblem eevLarge = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,dm.getXsolution());
            StochasticModelEvaluation eevmLarge = new StochasticModelEvaluation(eevLarge);
            eevmLarge.solve();
        
            
        
            //SP in large tree
            System.out.println("SP in large tree");
            StochasticModelEvaluationProblem spLargeEval = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,sm.getXsolution());
            StochasticModelEvaluation spmLarge = new StochasticModelEvaluation(spLargeEval);
            spmLarge.solve();
                       
            
            
            //VSS in large tree
            //System.out.println("VSS from large tree = "+(spmLarge.getObj() - eevmLarge.getObj()));
            average2 += spmLarge.getObj() - eevmLarge.getObj();
            if(max2 < spmLarge.getObj() - eevmLarge.getObj()){
                max2 = spmLarge.getObj() - eevmLarge.getObj();
            }
            if(min2 > spmLarge.getObj() - eevmLarge.getObj()){
                min2 = spmLarge.getObj() - eevmLarge.getObj();
            }
            */
           
        /* for buffer
            average1 += eevmSmall.getObj();
            if(max1 < eevmSmall.getObj()){
                max1 = eevmSmall.getObj();
            }
            if(min1 > eevmSmall.getObj()){
                min1 = eevmSmall.getObj();
            }
        */    
            run += 1;
        }
        
        System.out.println("VSS in small tree");
        System.out.println("Average: "+average1/nScenarioTrees);
        System.out.println("Max: "+max1);
        System.out.println("Min: "+min1);
    
        
        System.out.println("Rescheduling of deterministic solution in small trees (average)");
        System.out.println("Total: "+average3/nScenarioTrees);
        System.out.println("Prev: "+average4/nScenarioTrees);
        System.out.println("Corr: "+average5/nScenarioTrees);
        System.out.println("Mod: "+average6/nScenarioTrees);
        
        
        System.out.println("Rescheduling of stochastic solution in small trees (average)");
        System.out.println("Total: "+average7/nScenarioTrees);
        System.out.println("Prev: "+average8/nScenarioTrees);
        System.out.println("Corr: "+average9/nScenarioTrees);
        System.out.println("Mod: "+average10/nScenarioTrees);
        

        
        /*
        System.out.println("Evaluated in small trees");
        System.out.println("Average: "+average1/nScenarioTrees);
        System.out.println("Max: "+max1);
        System.out.println("Min: "+min1);
        
        //EEV in large tree
            
            StochasticModelEvaluationProblem eevLarge = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,dm.getXsolution());
            StochasticModelEvaluation eevmLarge = new StochasticModelEvaluation(eevLarge);
            eevmLarge.solve();
            System.out.println("Value in large tree: "+eevmLarge.getObj());
            
            
            
            
            buffer += 0.1;
        */
        
        //}
        
        capFullParam += 0.2;
        
        }
        //if(run > 3){
        /*
            //SP in small tree
            //System.out.println("SP in small tree");
            StochasticProblem sp = new StochasticProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nScenarios,values,punishments,A,capScenarios,hours,pi);
            StochasticModel spm = new StochasticModel(sp);
            spm.solve();
            int totalTasks = 0;
            int nPrev = 0;
            int nCorr = 0;
            int nMod =0;
            for(int i=1; i <= nBundles; i++){
                for(int t=1; t < nWeeks; t++){
                    if(spm.getXsolution(i, t)==1 ){
                        if(ID[i-1] <= 45){
                            nPrev += 1;
                            totalTasks += 1;
                        }else if(ID[i-1] <= 265 && endWeek[i-1] <= (currentWeek + 5) && currentYear == endYearModified[i-1]){
                            nCorr += 1;
                            totalTasks += 1;
                        }else if(ID[i-1] > 265 && (values[i-1][t-1]>0)){
                            nMod += 1;
                            totalTasks += 1;
                        }
                    }
                }
            }
            
            System.out.println("total "+totalTasks);
            System.out.println("preventive "+nPrev);
            System.out.println("corrective "+nCorr);
            System.out.println("modifications "+nMod);
            
            average1 += totalTasks;
            if(max1 < totalTasks){
                max1 = totalTasks;
            }
            if(min1 > totalTasks){
                min1 = totalTasks;
            }
            average2 += nPrev;
            if(max2 < nPrev){
                max2 = nPrev;
            }
            if(min2 > nPrev){
                min2 = nPrev;
            }
            average3 += nCorr;
            if(max3 < nCorr){
                max3 = nCorr;
            }
            if(min3 > nCorr){
                min3 = nCorr;
            }
            average4 += nMod;
            if(max4 < nMod){
                max4 = nMod;
            }
            if(min4 > nMod){
                min4 = nMod;
            }
        */
        //}   
            
        /*    
            //WS in small tree
            double countSmallTreeObj = 0;
            for(int s=1; s <= nScenarios; s++){
                EVPImodel evpim = new EVPImodel(sp,s);
                evpim.solve();
                countSmallTreeObj += evpim.getObj();
                //System.out.println(+countObj);
            }
            //System.out.println("WS in small tree= "+countSmallTreeObj);
            
            //EVPI small tree
            System.out.println("EVPI in small tree = "+(countSmallTreeObj - spm.getObj()));
        */    
            
        /*
            //VSS in small tree
            //System.out.println("VSS from small tree = "+(spm.getObj() - eevmSmall.getObj()));       
            average1 += spm.getObj() - eevmSmall.getObj();
            if(max1 < spm.getObj() - eevmSmall.getObj()){
                max1 = spm.getObj() - eevmSmall.getObj();
            }
            if(min1 > spm.getObj() - eevmSmall.getObj()){
                min1 = spm.getObj() - eevmSmall.getObj();
            }
        */
        /*
            //EEV in large tree
            //System.out.println("Value in large tree");
            StochasticModelEvaluationProblem eevLarge = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,dm.getXsolution());
            StochasticModelEvaluation eevmLarge = new StochasticModelEvaluation(eevLarge);
            eevmLarge.solve();
        
        
            //SP in large tree
            //System.out.println("SP in large tree");
            StochasticModelEvaluationProblem spLargeEval = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,spm.getXsolution());
            StochasticModelEvaluation spmLarge = new StochasticModelEvaluation(spLargeEval);
            spmLarge.solve();
        */
        /*    
            StochasticProblem spLargeTree = new StochasticProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD);
            
            //WS in large tree
            double countLargeTreeObj = 0;
            for(int d=1; d <= nData; d++){
                EVPImodel largeWS = new EVPImodel(spLargeTree,d);
                largeWS.solve();
                countLargeTreeObj += largeWS.getObj();
            }
            //System.out.println("WS in large tree = "+countLargeTreeObj);
            
            //EVPI in large tree
            System.out.println("EVPI in large tree = "+(countLargeTreeObj - spmLarge.getObj()));
        */    
        /*
            //VSS in large tree
            //System.out.println("VSS from large tree = "+(spmLarge.getObj() - eevmLarge.getObj()));
            average2 += spmLarge.getObj() - eevmLarge.getObj();
            if(max2 < spmLarge.getObj() - eevmLarge.getObj()){
                max2 = spmLarge.getObj() - eevmLarge.getObj();
            }
            if(min2 > spmLarge.getObj() - eevmLarge.getObj()){
                min2 = spmLarge.getObj() - eevmLarge.getObj();
            }
        */
        /*
            run += 1;
        }
        */
        /*
        capFullParam += 0.1;
        
        System.out.println("Stochastic solution");
        
        System.out.println("Total Average = "+(average1/nScenarioTrees));
        System.out.println("Total Max = "+max1);
        System.out.println("Total Min = "+min1);
        
        System.out.println("Prev Average = "+(average2/nScenarioTrees));
        System.out.println("Prev Max = "+max2);
        System.out.println("Prev Min = "+min2);
        
        
        System.out.println("Corr Average = "+(average3/nScenarioTrees));
        System.out.println("Corr Max = "+max3);
        System.out.println("Corr Min = "+min3);
        
        System.out.println("Mod Average = "+(average4/nScenarioTrees));
        System.out.println("Mod Max = "+max4);
        System.out.println("Mod Min = "+min4);
        
        */
        
        
        //}
        //buffer += 0.05;
        
        /*
        System.out.println("VSS in small tree");
        System.out.println("Average = "+(average1/nScenarioTrees));
        System.out.println("Max = "+max1);
        System.out.println("Min = "+min1);
        
        System.out.println("VSS in large tree");
        System.out.println("Average = "+(average2/nScenarioTrees));
        System.out.println("Max = "+max2);
        System.out.println("Min = "+min2);
        */
        
        
        /*
        //EEV in large tree
            //System.out.println("Value in large tree");
            StochasticModelEvaluationProblem eevLarge = new StochasticModelEvaluationProblem(nTasks,nBundles,nWeeks,ITindicator,nResources,nGroupedRes,RNindicator,nData,values,punishments,A,capacityAllData,hours,piD,dm.getXsolution());
            StochasticModelEvaluation eevmLarge = new StochasticModelEvaluation(eevLarge);
            eevmLarge.solve();
            System.out.println("Value in large tree = "+eevmLarge.getObj());
        */
        
        
        
    }
}
